This is the README file to support using semantic constraints in the semGCN retrofitting pipeline.

The repository includes the semantic relationships used in the original synGCN/semGCN paper and
additional semantic information extracted from VerbNet and FrameNet.

The semantic information files are in the format relation per line,
e.g. 'word1 word2'.

semantic_info/
	antonyms.txt - extracted from WordNet;
	framenet_verbs.txt - pairs of verbs eliciting the same FrameNet frame;
	hypernyms.txt - extracted from WordNet;
	hyponyms.txt - extracted from WordNet;
	synonyms.txt - extracted from PPDB;
	verbnet_verbs.txt - pairs of verbs sharing the same low-level VerbNet class. 
