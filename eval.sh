#!/bin/bash

###############################################################################################################
# COPYRIGHT (C) WLUPER LTD. - ALL RIGHTS RESERVED 2017 - Present
#
# UNAUTHORIZED COPYING, USE, REPRODUCTION OR DISTRIBUTION OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED.
# ALL CONTENTS ARE PROPRIETARY AND CONFIDENTIAL.
#
# WRITTEN BY:
#   Olga Majewska
#
# GENERAL ENQUIRIES:
#   <contact@wluper.com>
###############################################################################################################

###############################################################################################################
# This script iterates through embedding files on S3, runs evaluation on a chosen task and saves the results
# into a .csv file.
#
# Evaluation tasks: [relation_extraction, snli]
# Evaluation metrics: [accuracy, f1]
#
# Usage: sh eval.sh task_name path/to/embedding
#
# Note: path/to/embedding should not include the number of epochs at the end of the file name. The script loops
# through epochs 1-50 and appends epoch number to the end of the path:
# e.g. sh eval.sh relation_extraction embeddings/gloveS__e50__WN__27_08_2019_11:54:41 will run the evaluation
# for each of the embedding files, i.e. gloveS__e50__WN__27_08_2019_11:54:41_e1,
# gloveS__e50__WN__27_08_2019_11:54:41_e2, etc.
###############################################################################################################

path="./graph-spec/Extrinsic-Evaluation-tasks/"
task=$1
task_path=$path$task
cd
cd $task_path
counter=1
embedding=$2
linker="_e"
while [ $counter -le 50 ]
do 
  embedding_file="$embedding$linker$counter"
  wluper_aws --downloadS3 -b wluper-word-embeddings -f $embedding_file
  python3.6 preprocess.py $embedding_file
  python3.6 train.py $embedding_file
  rm $embedding_file
  counter=$(( $counter + 1 ))
done

echo Evaluation finished
