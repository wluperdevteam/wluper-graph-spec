This is the README file to support running intrinsic evaluation with evaluate_on_all.py
on a suite of intrinsic evaluation benchmarks.

##############################################################################################################

evaluate_on_all.py calculates embedding results against all available fast running
benchmarks in the repository and saves results as single row csv table.

Usage: ./evaluate_on_all -f <path to file> -p <format:word2vec,word2vec_bin,dict,glove> -o <path to output file> -c <clean non alphanumeric chars: True or False>
